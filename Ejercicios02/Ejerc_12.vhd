----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:     07/11/2022 
-- Design Name: 
-- Module Name:    Ejercicio3_12
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity Ejercicio3_12 is
    Port ( clk : in  STD_LOGIC;
           Q: out  STD_LOGIC_VECTOR (0 to 7));
end Ejercicio3_12;

architecture Behavioral of Ejercicio3_12 is
	signal aux : STD_LOGIC_VECTOR(0 to 7) := "00000000";
begin
	Q <= aux;
process(clk)
	begin
		if(clk'event and clk='1') then
			aux <= aux+1;
			if(aux = "11110101") then
				aux <= "00000000";
		end if;
			end if;
	end process;
end Behavioral;

