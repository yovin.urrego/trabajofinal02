
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Ejercicio3_13 is
    Port ( clk, x: in  STD_LOGIC;
				z : out STD_LOGIC);
end Ejercicio3_13;

architecture Behavioral of Ejercicio3_13 is
	type estados is (A, B, C ,D);
	signal edo_pres, edo_fut: estados := A;
begin
	proceso1 : process (edo_pres, x) begin
		case edo_pres is
--esatdo A
			when A => z <= '0';
				if x = '0' then
					edo_fut <= D;
				else
					edo_fut <= B;
				end if;
				--esatdo B
			when B => z <= '0';
				if x = '0' then
					edo_fut <= D;
				else
					edo_fut <= C;
				end if;
				--esatdo C
			when C => z <= '0';
				if x = '0' then
					edo_fut <= D;
				else
					edo_fut <= B;
				end if;
			when D =>
			--esatdo D
				if x = '0' then
					edo_fut <=D;
					z <= '0';
				else
					edo_fut <= A;
					z <= '1';
				end if;
		end case;
	end process proceso1;
proceso2: process (clk) begin
	if(clk'event and clk='1') then
		edo_pres <= edo_fut;
	end if;
end process proceso2;
end Behavioral;

