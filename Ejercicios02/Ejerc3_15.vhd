----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    18:05:02 07/11/2022 
-- Design Name: 
-- Module Name:    Registro4BitsConControl - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity Ejercicio3_15 is
port(
	CLK, MC, SENSOR: IN STD_LOGIC;
	INSE: INOUT STD_LOGIC;
	COLORS : OUT STD_LOGIC_VECTOR(0 TO 2)	
);
end Ejercicio3_15;

architecture behavior of Ejercicio3_15 is
	signal Z: STD_LOGIC_VECTOR(0 TO 2);
begin
	INSE<= not(Z(0) or Z(1) or Z(2));
	COLORS <=Z;
process (CLK)
	begin
	if (CLK ' EVENT and CLK = '0' and MC = '0' and SENSOR = '1') THEN 
		case Z IS
		-- 000= verde
		-- 100= ambar
		-- 010 = rojo
		when "000" => Z	<= "100" ;
		when "100" => Z	<= "010" ;
		when others => Z  <= "000" ; 
		end case;
	end if;
end process;
end behavior;

