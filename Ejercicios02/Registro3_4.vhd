----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    20:18:00 07/11/2022 
-- Design Name: 
-- Module Name:    Registro34 - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Registro34 is
port(
	CLK, MC: IN STD_LOGIC;
	INSE: INOUT STD_LOGIC;
	Q : OUT STD_LOGIC_VECTOR(0 TO 3)	
);
end Registro34;

architecture Behavioral of Registro34 is
signal Z: STD_LOGIC_VECTOR(0 TO 3);
begin
	INSE<= not(Z(0) or Z(1) or Z(2));
	Q <=Z;
process (CLK)
	begin
	if (CLK ' EVENT and CLK = '0' and MC = '0') THEN 
		case Z IS
		when "0000" => Z	<= "1000" ;
		when "1000" => Z	<= "0100" ;
		when "0100" => Z	<= "0010" ;
		when "0010" => Z	<= "0001" ;
		when "0001" => Z	<= "0000" ;
		when others => Z  <= "0000" ; 
		end case;
	end if;
end process;
end Behavioral;

