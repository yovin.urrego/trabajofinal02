----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    21:19:54 07/11/2022 
-- Design Name: 
-- Module Name:    Semaforo3_5 - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

----------------------------------------------------------------------------------
entity Semaforo3_5 is
port(
	CLK, MC: IN STD_LOGIC;
	INSE: INOUT STD_LOGIC;
	COLORS : OUT STD_LOGIC_VECTOR(0 TO 2)	
);
end Semaforo3_5;

architecture Behavioral of Semaforo3_5 is
signal Z: STD_LOGIC_VECTOR(0 TO 2);
begin
	INSE<= not(Z(0) or Z(1) or Z(2));
	COLORS <=Z;
process (CLK)
	begin
	if (CLK ' EVENT and CLK = '0' and MC = '0') THEN 
		case Z IS
		-- 000= verde
		-- 100= ambar
		-- 010 = rojo
		when "000" => Z	<= "100" ;
		when "100" => Z	<= "010" ;
		when others => Z  <= "000" ; 
		end case;
	end if;
end process;
end Behavioral;
