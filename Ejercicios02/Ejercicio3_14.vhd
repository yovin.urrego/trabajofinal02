----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    18:12:48 07/12/2022 
-- Design Name: 
-- Module Name:    Ejercicio3_14 - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Ejercicio3_14 is
port (clk : in  STD_LOGIC;
				verificado: in  STD_LOGIC;
				aux : inout  STD_LOGIC_VECTOR (2 downto 0));
end Ejercicio3_14;

architecture Behavioral of Ejercicio3_14 is
Type estado is (A, B, C, D, E,F,G,H);
signal estado_fut, estate: estado;
begin
process (estate, verificado) begin
case estate is 
--Estado A
when A => 
	aux <= "000";
	if verificado = '1' then estado_fut <= B;
	else estado_fut <= H;
	end if;
--Estado B	
when B =>
	aux <= "001";
	if verificado = '1' then estado_fut <= C;
	else estado_fut <= A;
	end if;
--Estado C
when C =>
	aux <= "011";
	if verificado = '1' then estado_fut <= D;
	else estado_fut <= B;
	end if;
--Estado D	
when D => 
	aux <= "010";
	if verificado = '1' then estado_fut <= E;
	else estado_fut <= C;
	end if;
--Estado E	
when E =>
aux <= "110";
	if verificado = '1' then estado_fut <= F;
	else estado_fut <= D;
	end if;
	--Estado F	
	when F =>
aux <= "111";
	if verificado = '1' then estado_fut <= G;
	else estado_fut <= E;
	end if;
	
	--Estado G
	when G =>
aux <= "101";
	if verificado = '1' then estado_fut <= H;
	else estado_fut <= F;
	end if;
	--Estado H
	when H =>
aux <= "110";
	if verificado = '1' then estado_fut <= A;
	else estado_fut <= G;
	end if;
	end case;
end process; 

process (clk) begin
if (clk' event and clk ='1') then
estate <= estado_fut;
end if;
end process;
end Behavioral;

