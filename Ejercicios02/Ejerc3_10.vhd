--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   
-- Design Name:   
-- Module Name:   C:/Users/USUARIO/Downloads/Arquitectura-de-ordenadores-main/Ejercicios02/TB_Semaforo3_5.vhd
-- Project Name:  Ejercicios02
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity Ejercicio3_10 is
    Port ( clk, C : in  STD_LOGIC;
           SAL1, SAL2 : inout  STD_LOGIC;
           Q : out  STD_LOGIC_VECTOR (3 downto 0));
end Ejercicio3_10;

architecture Behavioral of Ejercicio3_10 is
signal aux : STD_LOGIC_VECTOR (3 downto 0) := "0000";
begin
Q <= aux;
process(clk, C)
begin
	if (clk'event and clk = '1') then
		if (C = '0') then
			if(aux = "1001") then
				aux <= "0000";
			else
				aux <= aux+1;
			end if;
		else
			if(aux = "0000") then
				aux <= "1001";
			else
				aux <= aux-1;
			end if;
		end if;
		if (C = '0' and aux(0) = '1') then
			SAL1 <= '1';
			SAL2 <= '0';
		elsif(C = '1' and aux(0) = '0') then
			SAL1 <= '0';
			SAL2 <= '1';
		else
			SAL1 <= '0';
			SAL2 <= '0';
		end if;
	end if;
end process;
end Behavioral;

