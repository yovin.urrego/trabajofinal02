----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    15:34:20 07/11/2022 
-- Design Name: 
-- Module Name:    Ejercicio3_11
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity Ejercicio3_11 is
    Port ( clk : in  STD_LOGIC;
           Deco1, Deco2 : out  STD_LOGIC_VECTOR (0 to 6);
           SAL74931,  SAL74932 : out  STD_LOGIC_VECTOR (0 to 3));
end Ejercicio3_11;

architecture Behavioral of Ejercicio3_11 is
	signal aux1 : STD_LOGIC_VECTOR(0 to 3) := "0000";
	signal aux2 : STD_LOGIC_VECTOR(0 to 3) := "0000";
begin
	 SAL74931 <= aux1;
	 SAL74932<= aux2;
	process(clk)
	begin
		if(clk'event and clk = '1') then
			aux1 <= aux1+1;
			if(aux1 = "1001") then
				aux1<= "0000";
				aux2 <= aux2+1;
			end if;
			if (aux2 = "1001" and aux1 = "1001") then
				aux2 <= "0000";
			end if;
			case aux1 is
				when "0000" => Deco1 <= "0000000";
				when "0001" => Deco1 <= "0000001";
				when "0010" => Deco1 <= "0000010";
				when "0011" => Deco1  <= "0000011";
				when "0100" => Deco1  <= "0000100";
				when "0101" => Deco1  <= "0000101";
				when "0110" => Deco1  <= "0000110";
				when "0111" => Deco1  <= "0000111";
				when "1000" => Deco1  <= "0001000";
				when "1001" => Deco1  <= "0001001";
				when others => Deco1  <= "0000000";
			end case;
			case aux2 is
				when "0000" => Deco2 <= "0000000";
				when "0001" => Deco2 <= "0000001";
				when "0010" => Deco2<= "0000010";
				when "0011" => Deco2  <= "0000011";
				when "0100" => Deco2  <= "0000100";
				when "0101" => Deco2  <= "0000101";
				when "0110" => Deco2  <= "0000110";
				when "0111" => Deco2  <= "0000111";
				when "1000" => Deco2  <= "0001000";
				when "1001" => Deco2  <= "0001001";
				when others => Deco2  <= "0000000";
			end case;
		end if;
	end process;
end Behavioral;

