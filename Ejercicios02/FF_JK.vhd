----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    15:34:20 07/11/2022 
-- Design Name: 
-- Module Name:    FF_JK - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity flipFlopJK is
    port (
        status, notStatus : out std_logic;
        clock, j, k : in std_logic
    );
end entity;

architecture arch_flipFlopJK of flipFlopJK is

    signal internalQ : std_logic;

begin

    status <= internalQ;
    notStatus <= not internalQ;

    main_process : process (clock)
    begin

        if rising_edge (clock) then

            if j = '0' and k = '0' then
                internalQ <= internalQ;
            elsif j = '0' and k = '1' then
                internalQ <= '0';
            elsif j = '1' and k = '0' then
                internalQ <= '1';
            else
                internalQ <= not internalQ;
            end if;
        end if;

    end process;

end  arch_flipFlopJK;