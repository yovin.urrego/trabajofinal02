LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY TB_Ejercicio3_15 IS
END TB_Ejercicio3_15;
 
ARCHITECTURE behavior OF TB_Ejercicio3_15 IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT Ejercicio3_15
    port(
	CLK, MC, SENSOR: IN STD_LOGIC;
	INSE: INOUT STD_LOGIC;
	COLORS : OUT STD_LOGIC_VECTOR(0 TO 2)	
);
    END COMPONENT;
    
   -- No clocks detected in port list. Replace <clock> below with 
   -- appropriate port name 
 SIGNAL  CLK,MC, SENSOR: STD_LOGIC := '0';
 SIGNAL 	INSE:  STD_LOGIC:= '0' ;
 SIGNAL 	COLORS :  STD_LOGIC_VECTOR(0 TO 2):= (others => '0');
   constant CLK_period : time := 20000 ms;
 
BEGIN
	-- Instantiate the Unit Under Test (UUT)
   uut: Ejercicio3_15 PORT MAP (
	MC=> MC,
	SENSOR => SENSOR,
	INSE=> INSE,
	COLORS  =>COLORS ,
	CLK =>CLK
        );
   -- Clock process definitions
   CLK_process :process
   begin
		CLK <= '0';
		wait for CLK_period;
		CLK <= '1';
		wait for CLK_period;
		CLK <= '0';
		wait for CLK_period;
		CLK <= '1';
		wait for CLK_period;
		CLK <= '0';
		wait for CLK_period/4;
		CLK <= '1';
		wait for CLK_period/4;
   end process;
	
	stim_proc: process
   begin		
      -- hold reset state for 10 ns.
      MC <= '0';
		SENSOR <= '1';
      wait;
   end process;
END;
