--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   21:22:48 07/11/2022
-- Design Name:   
-- Module Name:   C:/Users/USUARIO/Downloads/Arquitectura-de-ordenadores-main/Ejercicios02/TB_Semaforo3_5.vhd
-- Project Name:  Ejercicios02
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: Semaforo3_5
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY TB_Semaforo3_5 IS
END TB_Semaforo3_5;
 
ARCHITECTURE behavior OF TB_Semaforo3_5 IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT Semaforo3_5
    port(
	CLK, MC: IN STD_LOGIC;
	INSE: INOUT STD_LOGIC;
	COLORS : OUT STD_LOGIC_VECTOR(0 TO 2)	
);
    END COMPONENT;
    
   -- No clocks detected in port list. Replace <clock> below with 
   -- appropriate port name 
 SIGNAL  CLK,MC: STD_LOGIC := '0';
 SIGNAL 	INSE:  STD_LOGIC:= '0' ;
 SIGNAL 	COLORS :  STD_LOGIC_VECTOR(0 TO 2):= (others => '0');
   constant CLK_period : time := 20000 ms;
 
BEGIN


	-- Instantiate the Unit Under Test (UUT)
   uut: Semaforo3_5 PORT MAP (
	MC=> MC,
	INSE=> INSE,
	COLORS  =>COLORS ,
	CLK =>CLK
        );
   -- Clock process definitions
   CLK_process :process
   begin
		CLK <= '0';
		wait for CLK_period;
		CLK <= '1';
		wait for CLK_period;
		CLK <= '0';
		wait for CLK_period;
		CLK <= '1';
		wait for CLK_period;
		CLK <= '0';
		wait for CLK_period/4;
		CLK <= '1';
		wait for CLK_period/4;
   end process;
	
	stim_proc: process
   begin		
      -- hold reset state for 10 ns.
      MC <= '0';
      wait;
   end process;
END;
