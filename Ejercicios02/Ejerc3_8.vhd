--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   
-- Design Name:   
-- Module Name:   C:/Users/USUARIO/Downloads/Arquitectura-de-ordenadores-main/Ejercicios02/TB_Semaforo3_5.vhd
-- Project Name:  Ejercicios02
-- Target Device:  
-- Tool versions:  
-- Description:   

-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity Ejercicio3_8 is
    Port ( clk, X : in  STD_LOGIC;
           Q : out  STD_LOGIC_VECTOR (1 downto 0));
end Ejercicio3_8;

architecture Behavioral of Ejercicio3_8 is
signal Qaux : STD_LOGIC_VECTOR (1 downto 0);
begin
	Q <= Qaux;
	process(clk)
	begin
		if (clk'event and clk = '1') then
			if (X = '0') then
				Qaux <= Qaux+1;
			else 
				Qaux <= Qaux - 1;
			end if;
		end if;
	end process;
end Behavioral;

