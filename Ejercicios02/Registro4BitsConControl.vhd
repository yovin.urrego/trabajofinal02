----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    18:05:02 07/11/2022 
-- Design Name: 
-- Module Name:    Registro4BitsConControl - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Registro4BitsConControl is
	port(CLK,DSR,DSL, RESET: IN STD_LOGIC;
	S : IN STD_LOGIC_VECTOR(0 TO 1);
	P : IN STD_LOGIC_VECTOR(0 TO 3);
	Q : OUT STD_LOGIC_VECTOR(0 TO 3)
);
end Registro4BitsConControl;

architecture Behavioral of Registro4BitsConControl is
signal Z: STD_LOGIC_VECTOR(0 TO 3);
begin
process (CLK, RESET)
begin
if(RESET = '0') then 
		Z <= "0000";
elsif (CLK ' EVENT and CLK = '1') THEN 
	case S  is
		when "01" => Z <= DSR & Z(0 TO 2) ;
		when "10" => Z <= DSL & Z(1 TO 3) ;
		when "11" => Z <= P;
		when others => Z <= "UUUU";
	end case;
end if;
end process;
end Behavioral;

