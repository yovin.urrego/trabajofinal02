----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    18:38:26 07/12/2022 
-- Design Name: 
-- Module Name:    Contador - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity Contador is
    Port ( clock : in  STD_LOGIC;
           Clear, Enable : in  STD_LOGIC;
           Dec,Uni : inout  STD_LOGIC_VECTOR (3 DOWNTO 0));
end Contador;

architecture Behavioral of Contador is
begin
  PROCESS(clock)
  begin
   if clock'event and clock = '1' then
    if Clear = '1' then
      Dec <= "0000"; Uni <= "0000";
   elsif Enable = '1' then
      if Uni = "1001" then 
          Uni <= "0000";
     if Dec = "1001" then
        Dec <= "0000";
     else
                Dec <= Dec + '1';
             end if;
   else
            Uni <= Uni +  '1';  
   end if;   
  end if;
     end if;  
  end PROCESS;   
 end  Behavioral;